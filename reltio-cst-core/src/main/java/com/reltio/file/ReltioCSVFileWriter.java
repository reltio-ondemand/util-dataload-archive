package com.reltio.file;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * 
 * @author Ganesh.Palanisamy@reltio.com Created : Sep 20, 2014
 */
public class ReltioCSVFileWriter implements ReltioFileWriter {

	private final CSVWriter cw;
	private final Set<String> crosswalkUnique = new HashSet<String>();
	CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();

	public ReltioCSVFileWriter(String fileName) throws IOException {
		encoder.onMalformedInput(CodingErrorAction.REPLACE);
		encoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
		FileOutputStream os = new FileOutputStream(fileName);
		os.write(0xef);
		os.write(0xbb);
		os.write(0xbf);
		cw = new CSVWriter(new OutputStreamWriter(os));
	}

	public ReltioCSVFileWriter(String fileName, String encoding)
			throws IOException {

		encoder.onMalformedInput(CodingErrorAction.REPORT);
		encoder.onUnmappableCharacter(CodingErrorAction.REPORT);
		cw = new CSVWriter(new OutputStreamWriter(
				new FileOutputStream(fileName), encoder));

	}

	public void writeToFile(List<String[]> lines) throws IOException {
		if (lines != null) {
			synchronized (cw) {
				for (String[] line : lines) {
					cw.writeNext(line);
				}
			}
		}
	}

	public void writeToFile(List<String[]> lines, boolean unique)
			throws IOException {
		if (unique) {
			if (lines != null) {
				synchronized (cw) {
					for (String[] line : lines) {
						if (crosswalkUnique.add(line[0] + line[1])) {
							cw.writeNext(line);

						}
					}
				}
			}

		} else {
			writeToFile(lines);
		}

	}

	public void writeToFile(String[] line) throws IOException {
		if (line != null) {
			synchronized (cw) {
				cw.writeNext(line);
			}
		}
	}

	public void close() throws IOException {
		synchronized (cw) {
			cw.close();
		}
	}

	@Override
	public void writeBulkToFile(List<String> lines) throws IOException {
		// TODO Not implemented

	}

	@Override
	public void writeToFile(String line) throws IOException {
		// TODO Not Implemented

	}
}
