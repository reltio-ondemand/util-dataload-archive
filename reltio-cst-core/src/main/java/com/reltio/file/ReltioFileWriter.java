package com.reltio.file;

import java.io.IOException;
import java.util.List;

/**
 * 
 * This is the interface for writing a file in bulk and one line
 *
 */
public interface ReltioFileWriter {

	/**
	 * Writes the data to the file in bulk
	 * 
	 * @param lines
	 * @throws IOException
	 */
	public void writeToFile(List<String[]> lines) throws IOException;

	/**
	 * Writes single line data to the file
	 * 
	 * @param line
	 * @throws IOException
	 */
	public void writeToFile(String[] line) throws IOException;

	/**
	 * Writes the data to the file in bulk without delimiters
	 * 
	 * @param lines
	 * @throws IOException
	 */
	public void writeBulkToFile(List<String> lines) throws IOException;

	/**
	 * Writes single line data to the file without delimiters
	 * 
	 * @param line
	 * @throws IOException
	 */
	public void writeToFile(String line) throws IOException;

	/**
	 * Closes the File stream and writes all the data in the memory to the file
	 * 
	 * @throws IOException
	 */
	public void close() throws IOException;
}
