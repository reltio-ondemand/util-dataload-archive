package com.reltio.cst.service;

import java.util.Map;

import com.reltio.cst.domain.HttpMethod;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;

public interface RestAPIService {

	public String get(String requestUrl, Map<String, String> requestHeaders)
			throws APICallFailureException, GenericException;

	public String post(String requestUrl, Map<String, String> requestHeaders,
			String requestBody) throws APICallFailureException,
			GenericException;

	public String put(String requestUrl, Map<String, String> requestHeaders,
			String requestBody) throws APICallFailureException,
			GenericException;

	public String delete(String requestUrl, Map<String, String> requestHeaders,
			String requestBody) throws APICallFailureException,
			GenericException;

	public String doExecute(String requestUrl,
			Map<String, String> requestHeaders, String requestBody,
			HttpMethod requestMethod) throws APICallFailureException,
			GenericException;

}
