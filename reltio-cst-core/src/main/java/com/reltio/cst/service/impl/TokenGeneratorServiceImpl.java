package com.reltio.cst.service.impl;

import java.util.HashMap;
import java.util.Map;

import com.reltio.cst.domain.AuthenticationResponse;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.properties.AuthenticationProperties;
import com.reltio.cst.service.RestAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.util.GenericUtilityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This class implements way to get the token from the Auth server
 * 
 *
 */
public class TokenGeneratorServiceImpl extends Thread implements
		TokenGeneratorService {
	private static Logger logger = LoggerFactory.getLogger(TokenGeneratorServiceImpl.class.getName());


	private String username;
	private String password;
	private String authURL;
	private Map<String, String> authHeaders = new HashMap<String, String>();
	private AuthenticationResponse authenticationResponse;
	private RestAPIService apiService = new SimpleRestAPIServiceImpl();
	private boolean isRunning;

	/**
	 * This constructor will store the user details and throws exception if it
	 * is invalid;
	 * 
	 * @param username
	 * @param password
	 * @param authURL
	 * @throws APICallFailureException
	 * @throws GenericException
	 */
	public TokenGeneratorServiceImpl(String username, String password,
			String authURL) throws APICallFailureException, GenericException {
		this.username = username;
		this.password = password;
		if (GenericUtilityService.checkNullOrEmpty(authURL)) {
			this.authURL = AuthenticationProperties.DEFAULT_AUTH_SERVER_URL;
		} else {
			this.authURL = authURL;
		}
		populateAuthHeaders();
		getNewToken();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.reltio.cst.service.TokenGeneratorService#startBackgroundTokenGenerator
	 * ()
	 */
	@Override
	public boolean startBackgroundTokenGenerator() {
		if (!isRunning) {
			isRunning = true;
			this.start();
			return true;
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.TokenGeneratorService#getToken()
	 */
	@Override
	public String getToken() throws APICallFailureException, GenericException {
		if (authenticationResponse == null) {
			getNewToken();
		}
		return authenticationResponse.getAccessToken();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.TokenGeneratorService#getNewToken()
	 */
	@Override
	public String getNewToken() throws APICallFailureException,
			GenericException {

		String authReqUrl = null;
		authReqUrl = authURL
				+ AuthenticationProperties.DEFAULT_AUTH_SERVER_QUERY_PARAM
				+ username + AuthenticationProperties.PASSWORD + password;

		String responseStr = doAuthAPICall(authReqUrl, 1);
		//logger.info("Auth Response ="+ responseStr);
		authenticationResponse = AuthenticationProperties.GSON.fromJson(
				responseStr, AuthenticationResponse.class);
		
		return authenticationResponse.getAccessToken();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.reltio.cst.service.TokenGeneratorService#stopBackgroundTokenGenerator
	 * ()
	 */
	@SuppressWarnings("deprecation")
	@Override
	public boolean stopBackgroundTokenGenerator() {

		if (isRunning && this.isAlive()) {
			logger.info("Background Token Generation process Stopped...");
			this.stop();
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {

		logger.info("Background Token Generation process Started...");

		// Pre-validation of authentication object
		if (authenticationResponse == null) {
			try {
				refreshToken();
			} catch (APICallFailureException e) {
				e.printStackTrace();
			} catch (GenericException e) {
				e.printStackTrace();
			}
		}

		// Infinite loop to run the thread in background for updating the token
		while (true) {
			try {

				// Wait for 4 mins
				Thread.sleep(240000);
				refreshToken();

			} catch (APICallFailureException e) {
				e.printStackTrace();
			} catch (GenericException e) {
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * @throws APICallFailureException
	 * @throws GenericException
	 */
	private void refreshToken() throws APICallFailureException,
			GenericException {
		if (authenticationResponse == null) {
			getNewToken();
		}

		String authReqUrl = authURL
				+ AuthenticationProperties.DEFAULT_AUTH_SERVER_REFRESH_TOKEN_QUERY_PARAMS
						.replaceAll(AuthenticationProperties.REFRESH_TOKEN,
								authenticationResponse.getRefreshToken());
		String responseStr = doAuthAPICall(authReqUrl, 1);
		authenticationResponse = AuthenticationProperties.GSON.fromJson(
				responseStr, AuthenticationResponse.class);

	}

	/**
	 * @param url
	 * @param retryCount
	 * @return
	 * @throws APICallFailureException
	 * @throws GenericException
	 */
	private String doAuthAPICall(String url, int retryCount)
			throws APICallFailureException, GenericException {
		String response = null;

		try {
			response = apiService.post(url, authHeaders, null);
		} catch (APICallFailureException e) {
			logger.error("Auth Call Failed: Error Code = "
					+ e.getErrorCode() + " |||| Error Message: "
					+ e.getErrorResponse());

			// Send the Exception if the failure due to invalid user credentials
			if (e.getErrorCode().equals(
					AuthenticationProperties.INVALID_USER_CREDENTIALS)) {
				throw e;
			}

			// Retry till count reaches to expected Retry limit
//			if (retryCount < AuthenticationProperties.RETRY_LIMIT) {
//				doAuthAPICall(url, ++retryCount);
//			}
            if (e.getErrorCode() == 401 && retryCount < AuthenticationProperties.RETRY_LIMIT) {
                logger.info("Retrying with new token..");
                doAuthAPICall(url, ++retryCount);
            } else if (e.getErrorCode() == 502 && retryCount < AuthenticationProperties.RETRY_LIMIT_FOR_502) {
                try {
                    long sleepTime = (long) (Math.pow(2,retryCount)-1)*1000;
                    logger.info("Retrying in "+sleepTime +" milliseconds..");
                    Thread.sleep(sleepTime);
                    doAuthAPICall(url, ++retryCount);
                } catch (InterruptedException ex) {
                    logger.error("Unexpected interruption exception.. " + ex.getMessage());
                }
            } else if (e.getErrorCode() == 503 && retryCount < AuthenticationProperties.RETRY_LIMIT_FOR_503) {
                try {

                    long sleepTime = (long) (Math.pow(2,retryCount)-1)*1000;
                    logger.info("Retrying in "+sleepTime +" milliseconds..");
                    Thread.sleep(sleepTime);
                    doAuthAPICall(url, ++retryCount);
                } catch (InterruptedException ex) {
                    logger.error("Unexpected interruption exception.. " + ex.getMessage());
                }
            } else if (e.getErrorCode() == 504 && retryCount < AuthenticationProperties.RETRY_LIMIT_FOR_504) {
                try {

                    long sleepTime = (long) (Math.pow(2,retryCount)-1)*1000;
                    logger.info("Retrying in "+sleepTime +" milliseconds..");
                    Thread.sleep(sleepTime);
                    doAuthAPICall(url, ++retryCount);
                } catch (InterruptedException ex) {
                    logger.error("Unexpected interruption exception.. " + ex.getMessage());
                }
            }

			throw e;

		} catch (GenericException e) {

			logger.error("Auth Call Failed Due to unexpected Exception: Error Message: "
							+ e.getExceptionMessage());

			// Retry till count reaches to expected Retry limit
			if (retryCount < AuthenticationProperties.RETRY_LIMIT) {
				doAuthAPICall(url, ++retryCount);
			}

			throw e;

		}

		return response;
	}

	/**
	 * This is util method to populate the required headers for get token
	 */
	private void populateAuthHeaders() {
		authHeaders.put(AuthenticationProperties.AUTH_SERVER_HEADER,
				AuthenticationProperties.AUTH_SERVER_BASIC_TOKEN);
		authHeaders.put(AuthenticationProperties.CONTENT_TYPE_HEADER,
				AuthenticationProperties.CONTENT_TYPE_JSON);
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the authURL
	 */
	public String getAuthURL() {
		return authURL;
	}

	/**
	 * @param authURL
	 *            the authURL to set
	 */
	public void setAuthURL(String authURL) {
		this.authURL = authURL;
	}

}
