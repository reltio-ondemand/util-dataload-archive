package com.reltio.cst.exception.handler;

/**
 * 
 * This is Generic Exception class for all unexpected exception cases
 * 
 *
 */
public class GenericException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1334542334828334582L;

	private String exceptionMessage;

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	public GenericException(String exceptionMessage) {

		this.exceptionMessage = exceptionMessage;
	}
}
