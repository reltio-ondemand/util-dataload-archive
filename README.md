
#Data Load Utility

## Description
The Data Loader is a tool for bulk loading entities, relations, and interactions into a reltio tenant in JSON form. The tool allows for the process tracking with email alerts and output redirection to a csv file when records fail to load. The tool is commonly used in sequence following the JSON Generator tool to prepare data for loading.

##Change Log

```
#!plaintext

Last Update Date: 06/30/2017
Version: 1.0.0
Description: Initial version
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-dataload/src/c431f56bce00446e438dce448adf9011b56504c1/QuickStart.md?at=master&fileviewer=file-view-default).


